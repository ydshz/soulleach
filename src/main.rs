#![feature(buf_read_has_data_left)]
#![feature(associated_const_equality)]
mod network;

use std::{net::{SocketAddr, IpAddr, Ipv4Addr}, io::{stdin, BufRead, stdout, Write}};
use network::client::{SoulseekClient, UserData};

fn main() -> Result<(), Box<dyn std::error::Error>>{
    let stdin = stdin();
    print!("Please enter username: ");
    stdout().flush()?;
    let username = stdin.lock().lines().next().unwrap()?;
    print!("Please enter password: ");
    stdout().flush()?;
    let password = stdin.lock().lines().next().unwrap()?;

    let adress = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(208, 76, 170, 59)), 2242);
    let mut client = SoulseekClient::new(adress, UserData::new(username, password))?;
    match client.login() {
        Ok(_) => println!("Login sucessfull"),
        _ => return Err(Box::new(std::io::Error::new(std::io::ErrorKind::PermissionDenied, "Invalid password or username entered.")))
    };

    match client.init_connection() {
        Ok(_) => println!("Server connection initialized."),
        _ => return Err(Box::new(std::io::Error::new(std::io::ErrorKind::ConnectionAborted, "Error initializing server connection.")))
    };

    //client.search_file("bathoy".to_string())?;
    loop {
        let message = client.await_message();
        match message {
            Ok(m) => println!("Received message with code {} sucessfully.", m.msg_code),
            _ => println!("Error receiving message")
        }    
    }
}
