#![allow(non_camel_case_types)]

use std::num::ParseIntError;

/*
 * Represents a type in the Soulseek protocol. Each type needs to be converted into a hex string
 * before communication.
 * */
pub trait SoulseekType {
    fn get_value(&self) -> Vec<u8>;
    fn to_hex(&self) -> String {
        self.get_value()
            .into_iter()
            .map(|f| format!("{:02x}", f))
            .collect()
    }
}

pub struct Slsk_Int8 {
    value: [u8; 1],
}

#[allow(dead_code)]
impl Slsk_Int8 {
    pub fn new(v: u8) -> Slsk_Int8 {
        Slsk_Int8 {
            value: v.to_le_bytes(),
        }
    }
    pub fn from_hex(s: &str) -> Option<Slsk_Int8> {
        let bytes = decode_hex(s).ok()?;
       Some(Slsk_Int8 { value: [*bytes.get(0)?]})
    }
}

impl SoulseekType for Slsk_Int8 {
    fn get_value(&self) -> Vec<u8> {
        self.value.to_vec()
    }
}

pub struct Slsk_Int16 {
    value: [u8; 2],
}

#[allow(dead_code)]
impl Slsk_Int16 {
    pub fn new(v: u16) -> Slsk_Int16 {
        Slsk_Int16 {
            value: v.to_le_bytes(),
        }
    }
    pub fn from_hex(s: &str) -> Option<Slsk_Int16> {
        let bytes = decode_hex(s).ok()?;
        Some(Slsk_Int16 { value: [*bytes.get(0)?, *bytes.get(1)?]})
    }

}

impl SoulseekType for Slsk_Int16 {
    fn get_value(&self) -> Vec<u8> {
        self.value.to_vec()
    }
}

pub struct Slsk_Int32 {
    value: [u8; 4],
}

#[allow(dead_code)]
impl Slsk_Int32 {
    pub fn new(v: u32) -> Slsk_Int32 {
        Slsk_Int32 {
            value: v.to_le_bytes(),
        }
    }
    pub fn from_hex(s: &str) -> Option<Slsk_Int32> {
        let bytes = decode_hex(s).ok()?;
        Some(Slsk_Int32 { value: [*bytes.get(0)?, *bytes.get(1)?, *bytes.get(2)?, *bytes.get(3)?]})
    }

}

impl SoulseekType for Slsk_Int32 {
    fn get_value(&self) -> Vec<u8> {
        self.value.to_vec()
    }
}

pub struct Slsk_Int64 {
    value: [u8; 8],
}

#[allow(dead_code)]
impl Slsk_Int64 {
    pub fn new(v: u64) -> Slsk_Int64 {
        Slsk_Int64 {
            value: v.to_le_bytes(),
        }
    }
    pub fn from_hex(s: &str) -> Option<Slsk_Int64> {
        let bytes = decode_hex(s).ok()?;
        Some(Slsk_Int64 { value: [*bytes.get(0)?, *bytes.get(1)?, *bytes.get(2)?, *bytes.get(3)?, *bytes.get(4)?, *bytes.get(5)?, *bytes.get(6)?, *bytes.get(7)?]})
    }
}

impl SoulseekType for Slsk_Int64 {
    fn get_value(&self) -> Vec<u8> {
        self.value.to_vec()
    }
}

pub struct Slsk_Bool {
    value: bool,
}

#[allow(dead_code)]
impl Slsk_Bool {
    pub fn from_hex(s: &str) -> Option<Slsk_Bool> {
        let value = match s.chars().nth(0)? {
            '0' => false,
            '1' => true,
            _ => return None
        };
        Some(Slsk_Bool { value })
    }
    pub fn new(val: bool) -> Slsk_Bool {
        Slsk_Bool { value: val }
    }
}

impl SoulseekType for Slsk_Bool {
    fn get_value(&self) -> Vec<u8> {
        vec![self.value as u8]
    }

    fn to_hex(&self) -> String {
        (self.value as u32).to_string()
    }
}

pub struct Slsk_String {
    length: Slsk_Int32,
    value: Vec<u8>,
}

#[allow(dead_code)]
impl Slsk_String {
    pub fn new(v: String) -> Slsk_String {
        Slsk_String {
            length: Slsk_Int32::new(v.chars().count() as u32),
            value: v.as_bytes().to_vec(),
        }
    }
    pub fn from_hex(s: &str) -> Option<Slsk_String> {
        let length = Slsk_Int32::from_hex(&s[..8])?;
        let value = decode_hex(&s[8..]).ok()?;
        Some(Slsk_String { length, value })
    }
}

impl SoulseekType for Slsk_String {
    fn get_value(&self) -> Vec<u8> {
        self.value.to_owned()
    }
    fn to_hex(&self) -> String {
        self.length.to_hex()
            + &self
                .value
                .to_owned()
                .into_iter()
                .map(|f| format!("{:x}", f))
                .collect::<String>()
    }
}

fn decode_hex(s: &str) -> Result<Vec<u8>, ParseIntError> {
    (0..s.len())
        .step_by(2)
        .map(|i| u8::from_str_radix(&s[i..i + 2], 16))
        .collect()
}

#[allow(dead_code)]
#[derive(Clone, Debug)]
pub enum TypeOption {
    Slsk_Bool,
    Slsk_Int8,
    Slsk_Int16,
    Slsk_Int32,
    Slsk_Int64,
    Slsk_String,
}
