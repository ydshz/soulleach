use std::{u32, usize};
use super::types::{TypeOption, Slsk_Bool, SoulseekType, Slsk_Int8, Slsk_Int16, Slsk_Int32, Slsk_Int64, Slsk_String};
use super::messages::Serialize;
use std::u8;

pub trait Parsable : Serialize {
    fn parse_string(s: &str) -> Option<Box<Self>>;
}

/**
 * Function that parses the individual parameters of a message specified by the list of parameters
 * to parse.
 */
pub fn parse_params(msg: &str, types: Vec<TypeOption>) -> Option<Vec<Box<dyn SoulseekType>>> {
    let mut type_start = 0;
    let mut result = vec![];
    let mut remaining_types = types.to_owned();
    while remaining_types.len() != 0 {
        result.push(match *remaining_types.get(0)? {
            TypeOption::Slsk_Bool => Box::new(Slsk_Bool::from_hex(&msg[type_start..type_start+1])?) as Box<dyn SoulseekType>,
            TypeOption::Slsk_Int8 => Box::new(Slsk_Int8::from_hex(&msg[type_start..type_start+2])?) as Box<dyn SoulseekType>,
            TypeOption::Slsk_Int16 => Box::new(Slsk_Int16::from_hex(&msg[type_start..type_start+4])?) as Box<dyn SoulseekType>,
            TypeOption::Slsk_Int32 => Box::new(Slsk_Int32::from_hex(&msg[type_start..type_start+8])?) as Box<dyn SoulseekType>,
            TypeOption::Slsk_Int64 => Box::new(Slsk_Int64::from_hex(&msg[type_start..type_start+16])?) as Box<dyn SoulseekType>,
            TypeOption::Slsk_String => {
                let bytes = Slsk_Int32::from_hex(&msg[type_start..type_start+8])?.get_value();
                let length = u32::from_le_bytes( [*bytes.get(0)?, *bytes.get(1)?, *bytes.get(2)?, *bytes.get(3)?]) as usize;
                Box::new(Slsk_String::from_hex(&msg[type_start..type_start+8+(length*2)])?) as Box<dyn SoulseekType>
            }
            
        });
        type_start += match *remaining_types.get(0)? {
            TypeOption::Slsk_Bool => 1,
            TypeOption::Slsk_Int8 => 2,
            TypeOption::Slsk_Int16 => 4,
            TypeOption::Slsk_Int32 => 8,
            TypeOption::Slsk_Int64 => 16,
            TypeOption::Slsk_String => {
                let bytes = Slsk_Int32::from_hex(&msg[type_start..type_start+8])?.get_value();
                let length = u32::from_le_bytes( [*bytes.get(0)?, *bytes.get(1)?, *bytes.get(2)?, *bytes.get(3)?]) as usize;
                8+(length*2)
            }
        };
        remaining_types.remove(0);
    }
    Some(result)

}

/**
 * Function to convert hex string of low endian bytes into a 32 bit unsigned integer.
 */
pub fn hex_to_int32(string: &str) -> Result<u32, std::io::Error> {
    let bytes: Vec<u8> = hex::decode(string).map_err(|_op| std::io::Error::new(std::io::ErrorKind::InvalidData, "Parsing Error"))?;
    if bytes.len() != 4 {
        return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Parsing Error"));
    }
    let fixed_bytes = [*bytes.get(0).unwrap(), *bytes.get(1).unwrap(), *bytes.get(2).unwrap(), *bytes.get(3).unwrap()];
    let int = u32::from_le_bytes(fixed_bytes);
    Ok(int)
}

pub fn decimal_to_hex_string(values: &Vec<u8>) -> String {
    let hex_values = values.iter().map(|f| format!("{:x}", f)).map(|s| if s.len() == 1 {"0".to_string() + &s} else {s.to_owned()}).collect::<Vec<String>>();
    hex_values.join("")
}

pub fn hex_to_decimal(value: String) -> Result<Vec<u8>, std::io::Error> {
    let hex_values = value.chars().collect::<Vec<char>>().chunks(2).map(|c| c.iter().collect::<String>()).collect::<Vec<String>>();
    let mut decimal_values = vec![];
    for value in hex_values {
        decimal_values.push(u8::from_str_radix(&value, 16).map_err(|_op| std::io::Error::new(std::io::ErrorKind::InvalidData, "Failed parsing message"))?);
    }
    Ok(decimal_values)
}

pub trait MessageParser : Sync + Send{
    fn parse_message(&self, msg: &str) -> Option<Vec<Box<dyn SoulseekType>>>;
}

pub struct LoginParser {
}

impl MessageParser for LoginParser {
    fn parse_message(&self, msg: &str) -> Option<Vec<Box<dyn SoulseekType>>> {
        if msg.chars().nth(1)? == '0' {
            parse_params(&msg[1..], vec![TypeOption::Slsk_Bool, TypeOption::Slsk_String])
        } else {
            parse_params(&msg[1..], vec![TypeOption::Slsk_Bool, TypeOption::Slsk_String, TypeOption::Slsk_Int32, TypeOption::Slsk_String, TypeOption::Slsk_Bool])
        }
    }
}
