use crate::network::{types::{Slsk_Int32, Slsk_String, SoulseekType, Slsk_Bool}, parser::{self, decimal_to_hex_string, MessageParser, LoginParser, hex_to_decimal}};
use super::{Serialize, Deserialize};
use std::{io::{self, Write, Read}, collections::HashMap};
use md5;
use lazy_static::lazy_static;

lazy_static! {
    static ref MESSAGE_PARAMS: HashMap<isize, Box<dyn MessageParser>> = {
        let mut map = HashMap::new();
        map.insert(1, Box::new(LoginParser{}) as Box<dyn MessageParser>);
        map
    };
}

pub struct  ServerMessage {
    pub msg_code: isize,
    pub data: Vec<Box<dyn SoulseekType>>
}

impl Serialize for ServerMessage {
    // Serialize message to bytes
    fn serialize(&self, buf: &mut impl Write) -> io::Result<usize> {
        let serialized_data: String = self.data.iter().map(|f| f.to_hex()).collect();
        let msg_code = Slsk_Int32::new(self.msg_code as u32).to_hex();
        let length = Slsk_Int32::new((serialized_data.chars().count() / 2 + msg_code.chars().count() / 2 ) as u32).to_hex();
        let message = length + &msg_code + &serialized_data;
        buf.write_all(&hex_to_decimal(message.to_owned())?)?;
        Ok(message.len())
    }
}

impl Deserialize for ServerMessage {
    type Output = Self;

    fn deserialize(buf: &mut impl Read) -> io::Result<Self::Output> {
        let mut length_buffer = [0; 4];
        let mut msg_code_buffer = [0; 4];

        buf.read_exact(&mut length_buffer)?;
        let lenght = parser::hex_to_int32(&decimal_to_hex_string(&length_buffer.to_vec()))?;

        buf.read_exact(&mut msg_code_buffer)?;
        let msg_code = parser::hex_to_int32(&decimal_to_hex_string(&msg_code_buffer.to_vec()))? as isize;

        let mut param_buffer = vec![0; (lenght-4) as usize];
        buf.read_exact(&mut param_buffer)?;

        if MESSAGE_PARAMS.contains_key(&msg_code) {
            let type_content =  decimal_to_hex_string(&param_buffer);
            let types = MESSAGE_PARAMS.get(&msg_code).unwrap().parse_message(&type_content).ok_or(std::io::Error::new(std::io::ErrorKind::InvalidData, "Failed parsing message"))?; 
            Ok(ServerMessage { msg_code, data: types })
        } else {
            Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Failed parsing message"))
        }
    }
}

impl ServerMessage {
    /**
     * A login message consists of the following parameters:
     * username + password + version_number + hash + minor_version
     * version_number and minor_version are hardcoded magic numbers and hash is the md5 hash based
     * of the username concanted with the password.
     */
    pub fn build_login_message(username: String, password: String) -> ServerMessage {
       let hash = format!("{:x}", md5::compute(username.to_owned() + &password));
       ServerMessage { msg_code: 1, data: vec![Box::new(Slsk_String::new(username)), Box::new(Slsk_String::new(password)), Box::new(Slsk_Int32::new(160)), Box::new(Slsk_String::new(hash)), Box::new(Slsk_Int32::new(1))] }
    }

    /**
     * Message to specify the client port send on login. Parameters:
     * port + unknown + obfuscated_port
     * Where port and obfuscated_port specify the port on the client and unknown holds the magic
     * number 1.
     */
    pub fn build_set_wait_port_message(port: u32) -> ServerMessage {
        ServerMessage { msg_code: 2, data: vec![Box::new(Slsk_Int32::new(port)), Box::new(Slsk_Int32::new(1)), Box::new(Slsk_Int32::new(port))] }
    }

    /**
     * Message to search for a file. Parameters:
     * token + search_query
     * The token is randomly generated on the client to track the response.
     */
    pub fn build_file_search_message(search_query: String, token: u32) -> ServerMessage {
        ServerMessage {msg_code: 26, data: vec![Box::new(Slsk_Int32::new(token)), Box::new(Slsk_String::new(search_query))]}
    }

    /**
     * Message to check the privileges of the current user.
     */
    pub fn build_check_privileges_message() -> ServerMessage {
        ServerMessage {msg_code: 92, data: vec![]}
    }

    /**
     * Message to set the status of the current user.
     */
    pub fn build_set_status_message(online_status: OnlineStatus) -> ServerMessage {
        ServerMessage {msg_code: 28, data: vec![Box::new(Slsk_Int32::new(online_status as u32))]}
    }

    /**
     * Message to tell the server how many directories and files we share.
     */
    pub fn build_shared_folders_files_message(dir_count: u32, file_count: u32) -> ServerMessage {
        ServerMessage { msg_code: 35, data: vec![Box::new(Slsk_Int32::new(dir_count)), Box::new(Slsk_Int32::new(file_count))] }
    }

    /**
     * Message to track a user.
     */
    pub fn build_add_user_message(username: String) -> ServerMessage{
        ServerMessage { msg_code: 5, data: vec![Box::new(Slsk_String::new(username))]}
    }

    /**
     * Message to toggle parent messages.
     */
    pub fn build_toggle_parent_search_message(toggle: bool) -> ServerMessage {
        ServerMessage { msg_code: 71, data: vec![Box::new(Slsk_Bool::new(toggle))]}
    }

    /**
     * Message to notify the server about our branch root.
     */
    pub fn build_branch_root_message(username: String) -> ServerMessage {
        ServerMessage { msg_code: 127, data: vec![Box::new(Slsk_String::new(username))] }
    }

    /**
     * Message to notify the server which branch level we are in the distributed network.
     */
    pub fn build_branch_level_message(level: u32) -> ServerMessage {
        ServerMessage { msg_code: 126, data: vec![Box::new(Slsk_Int32::new(level))] }
    }

    /**
     * Message to tell the server if we accept children.
     */
    pub fn build_accept_children_message(acccept: bool) -> ServerMessage {
        ServerMessage { msg_code: 100, data: vec![Box::new(Slsk_Bool::new(acccept))] }
    }

    /**
     * Ping message to tell the server that we are still alive.
     */
    pub fn build_ping_message() -> ServerMessage {
        ServerMessage { msg_code: 28, data: vec![] }
    }
}

pub enum OnlineStatus {
    Away = 1,
    Online = 2
}
