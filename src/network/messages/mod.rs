pub mod server;

use std::io::{self, Write, Read};

pub trait Serialize {
    fn serialize(&self, buf: &mut impl Write) -> io::Result<usize>;
}

pub trait Deserialize {
    /// The type that this deserializes to
    type Output;

    /// Deserialize from a `Read`able buffer
    fn deserialize(buf: &mut impl Read) -> io::Result<Self::Output>;
}
