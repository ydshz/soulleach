use std::net::SocketAddr;
use std::net::TcpStream;
use std::io::{prelude::*, self, BufReader};
use rand::distributions::Distribution;
use rand::distributions::Uniform;
use rand::thread_rng;

use super::messages::server::ServerMessage;
use super::messages::{Serialize, Deserialize};

/**
 * A simple Tcp client that allows sending and receiving messages.
 */
struct TcpClient {
    reader: BufReader<TcpStream>,
    stream: TcpStream,
}

impl TcpClient {
    // Create new client based on a TcpStream
    pub fn with_stream(stream: TcpStream) -> io::Result<Self> {
        Ok(Self {
            reader: BufReader::new(stream.try_clone()?),
            stream,
        })
    }
    // Connect the client to the given adress
    pub fn connect(dest: SocketAddr) -> io::Result<Self> {
        let stream = TcpStream::connect(dest)?;
        Self::with_stream(stream)
    }

    // Send a message over the client
    pub fn send_message(&mut self, message: &impl Serialize) -> io::Result<()> {
        message.serialize(&mut self.stream)?;
        self.stream.flush()
    }

    // Read a message from the TcpStream
    pub fn read_message<T: Deserialize>(&mut self) -> io::Result<T::Output> {
        T::deserialize(&mut self.reader)
    }
}

/**
 * Struct responsable for interacting with the soulseek servers. Based on the TcpClient struct.
 */
pub struct SoulseekClient {
    tcp_client: TcpClient,
    user_data: UserData,
    logged_in: bool,
    intialized: bool,
}

impl SoulseekClient {
    #[allow(unused_mut)]
    pub fn new(adress: SocketAddr, user_data: UserData) -> io::Result<SoulseekClient>{
        let mut tcp_client = TcpClient::connect(adress)?;
        Ok(SoulseekClient { tcp_client , user_data, logged_in: false, intialized: false })
    }

    pub fn login(&mut self) -> io::Result<()> {
       let login_message = ServerMessage::build_login_message(self.user_data.username.to_owned(), self.user_data.password.to_owned()); 
       self.tcp_client.send_message(&login_message)?;
       let response = self.await_message()?;
       if response.msg_code != 1 {
            Err(std::io::Error::new(io::ErrorKind::InvalidData, "Server did not respond correctly to the login message."))
       } else {
            match response.data.get(0).unwrap().get_value().get(0).unwrap() {
                1 => {
                    self.logged_in = true;
                    Ok(())
                },
                _ => {
                    Err(std::io::Error::new(io::ErrorKind::InvalidData, "Server failed logging in."))
                }
            }
       }
    }

    pub fn set_wait_port(&mut self) -> io::Result<()> {
        let set_wait_port_message = ServerMessage::build_set_wait_port_message(2234);
        self.tcp_client.send_message(&set_wait_port_message)
    }

    /**
     * Blocks the current thread until a message is received and handeled.
     */
    pub fn await_message(&mut self) -> io::Result<ServerMessage> {
        self.tcp_client.read_message::<ServerMessage>()
    }

    /**
     * Sends a request to the server to search for a specific file
     */
    pub fn search_file(&mut self, search_query: String) -> io::Result<()> {
        if !self.intialized {
            return Err(std::io::Error::new(io::ErrorKind::PermissionDenied, "Cant interact with the Soulseek server when not logged in."));
        }
        let mut rng = thread_rng();
        let rand_range = Uniform::new(0, u32::MAX);
        let token = rand_range.sample(&mut rng);
        let msg = ServerMessage::build_file_search_message(search_query, token);
        self.tcp_client.send_message(&msg)
    }

    /**
     * This function should be called after a sucessful login to tell the server specific
     * information about the client.
     */
    pub fn init_connection(&mut self) -> io::Result<()> {
        if !self.logged_in {
            return Err(std::io::Error::new(io::ErrorKind::PermissionDenied, "Cant interact with the Soulseek server when not logged in."));
        }
        // Tell the server required information about us
        self.tcp_client.send_message(&ServerMessage::build_check_privileges_message())?;
        self.tcp_client.send_message(&ServerMessage::build_set_wait_port_message(2234))?;
        self.tcp_client.send_message(&ServerMessage::build_set_status_message(super::messages::server::OnlineStatus::Online))?;
        self.tcp_client.send_message(&ServerMessage::build_shared_folders_files_message(0, 0))?;
        self.tcp_client.send_message(&ServerMessage::build_add_user_message(self.user_data.username.to_owned()))?;

        // Tell the server that we have no parent
        self.tcp_client.send_message(&ServerMessage::build_toggle_parent_search_message(true))?;
        self.tcp_client.send_message(&ServerMessage::build_branch_root_message(self.user_data.username.to_owned()))?;
        self.tcp_client.send_message(&ServerMessage::build_branch_level_message(0))?;
        self.tcp_client.send_message(&ServerMessage::build_accept_children_message(false))?;

        self.ping()?;
        self.intialized = true;
        Ok(())
    }

    /**
     * Send the server a single ping message.
     */
    pub fn ping(&mut self) -> io::Result<()> {
        if !self.logged_in {
            return Err(std::io::Error::new(io::ErrorKind::PermissionDenied, "Cant interact with the Soulseek server when not logged in."));
        }


        self.tcp_client.send_message(&ServerMessage::build_ping_message())
    }
}

#[derive(Clone, Debug, impl_new::New)]
pub struct UserData {
    username: String,
    password: String
}

